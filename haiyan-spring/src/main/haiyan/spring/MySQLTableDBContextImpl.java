package haiyan.spring;

import javax.sql.DataSource;

import haiyan.common.DebugUtil;
import haiyan.common.exception.Warning;
import haiyan.common.intf.database.IDatabase;
import haiyan.common.intf.database.sql.ISQLDatabase;
import haiyan.common.intf.exp.IExpUtil;
import haiyan.config.util.ConfigUtil;
import haiyan.database.JNDIDatabase;
import haiyan.exp.ExpUtil;
import haiyan.orm.database.sql.MySqlDBManager;
import haiyan.orm.database.sql.TableDBContext;
import haiyan.orm.intf.session.ITableDBContext;

public class MySQLTableDBContextImpl extends TableDBContext implements ITableDBContext {
	private DataSource dataSource;
	public DataSource getDataSource() {
		return dataSource;
	}
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		try {
			init();
		} catch (Throwable e) {
			throw Warning.getWarning(e);
		}
	}
	private void init() throws Throwable {
		IExpUtil exp = new ExpUtil(this);
		this.setExpUtil(exp);
		//String dbType = database.getDBType();
		String DSN = ConfigUtil.getDefaultDSN();
		IDatabase database = new JNDIDatabase(dataSource);
		database.setDSN(DSN);
		this.setDBM(new MySqlDBManager((ISQLDatabase)database) {
			@Override
			public void print(String s){
				DebugUtil.info(s);
			}
		});
		TableDBContextUtils.set(this);
	}
	@Override
	public void commit() {
		try {
			super.commit();
		} catch (Throwable e) {
			throw Warning.getWarning(e);
		}
//		throw new UnsupportedOperationException("Manual commit is not allowed over a Spring managed SqlSession");
	}
	@Override
	public void rollback() {
		try {
			super.rollback();
		} catch (Throwable e) {
			throw Warning.getWarning(e);
		}
//		throw new UnsupportedOperationException("Manual rollback is not allowed over a Spring managed SqlSession");
	}
	@Override
	public void close() {
		TableDBContextUtils.remove();
		try {
			super.close();
		} catch (Throwable e) {
			throw Warning.getWarning(e);
		}
//		throw new UnsupportedOperationException("Manual close is not allowed over a Spring managed SqlSession");
	}
}
//@Override
//public void clear() {
//	tableDBContextProxy.clear();
//}
//@Override
//public void begin() throws NotSupportedException, SystemException {
//	
//	System.out.println("TransactionManager");
//}
//@Override
//public int getStatus() throws SystemException {
//	
//	return 0;
//}
//@Override
//public Transaction getTransaction() throws SystemException {
//	
//	return null;
//}
//@Override
//public void resume(Transaction arg0) throws IllegalStateException, InvalidTransactionException, SystemException {
//	
//	System.out.println("TransactionManager");
//}
//@Override
//public void setRollbackOnly() throws IllegalStateException, SystemException {
//	
//	System.out.println("TransactionManager");
//}
//@Override
//public void setTransactionTimeout(int arg0) throws SystemException {
//	
//	System.out.println("TransactionManager");
//}
//@Override
//public Transaction suspend() throws SystemException {
//	
//	return null;
//}