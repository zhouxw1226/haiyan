package haiyan.spring;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

import haiyan.common.CloseUtil;
import haiyan.orm.intf.session.ITableDBContext;

//public static final String POINTCUT = "execution(* test.dao.*.update(..)) "
//		+ " || execution(* test.dao.*.insert(..)) "
//		+ " || execution(* test.dao.*.delete(..))";
public abstract class TransactionCommonAspect {
    protected ITableDBContext getContext(JoinPoint jp) {
    	return TableDBContextUtils.get();
	}
	@SuppressWarnings("unused")
	private void anyMethod(){} // 定义一个切入点  只是标识不进行调用
	//@Before(POINTCUT)
	public boolean before(JoinPoint jp) {
//		Object[] args = pjp.getArgs();
//		HttpServletRequest req = (HttpServletRequest) args[0];
//		HttpServletResponse res = (HttpServletResponse) args[1];
//		System.out.println("前置通知:"+pjp);
//		System.out.println("args.length:"+args.length);
		System.out.println("进入Before通知:"+jp+"\n");
		//DataSourceUtils.
		ITableDBContext context = getContext(jp);
		if (context!=null){
			try {
				context.openTransaction();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	//@Around(POINTCUT)
	public Object around(ProceedingJoinPoint jp) throws Throwable { // JoinPoint
		System.out.println("进入Around通知:"+jp+"\n");
		Object object = jp.proceed(); // 执行该方法
		return object;
	}
	//@AfterReturning(POINTCUT)  
	public void afterReturning(JoinPoint jp){  
		System.out.println("进入AfterReturning通知:"+jp+"\n");
		ITableDBContext context = getContext(jp);
		if (context!=null){
			try {
				context.commit();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}
	//@AfterThrowing(pointcut=POINTCUT,throwing="ex")
		public void afterThrowing(JoinPoint jp) {
		System.out.println("进入AfterThrowing通知:"+jp+"\n");
		ITableDBContext context = getContext(jp);
		if (context!=null){
			try {
				context.rollback();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
		}
	//@After(POINTCUT)
	public void after(JoinPoint jp) {
		System.out.println("进入After通知:" + jp + "\n");
//		TableDBContextUtils.remove();
		ITableDBContext context = getContext(jp);
//		DataSourceUtils.closeConnectionIfNecessary(con, context); 
		CloseUtil.close(context);
	}
	//@Invoke(POINTCUT)
	//public void invoke(JoinPoint joinPoint) { // 覆盖实现
//		System.out.println("进入Invoke通知:"+jp+"\n");
	//}
	//@After(POINTCUT)  
	//public void after(JoinPoint jp){  
//		System.out.println("进入After通知:"+jp+"\n");
	//}
	//System.out.println("退出Wrapper通知:"+jp+"\n");
	//Object target = jp.getTarget();
	//ITableDBContext context = null;
	//try {
//		if (target instanceof TestSpringDao) { //  && pjp.toString().indexOf(".update(")>=0
//			context = ((TestSpringDao)target).getTableContext();
//		}
//		if (context!=null){
//			context.openTransaction();
//		}
//		Object object = jp.proceed(); // 执行该方法
//		if (context!=null){
//			context.commit();
//		}
//		return object;
	//}catch(Throwable e){
////		if (context!=null){
////			context.rollback();
////		}
//		throw e;
	//}finally{
//		System.out.println("退出Wrapper通知:"+jp+"\n");
	//}
}
