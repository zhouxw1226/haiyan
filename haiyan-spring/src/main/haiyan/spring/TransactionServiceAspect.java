package haiyan.spring;

import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class TransactionServiceAspect extends TransactionCommonAspect { // 自动切面
}