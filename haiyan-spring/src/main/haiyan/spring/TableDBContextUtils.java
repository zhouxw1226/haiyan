package haiyan.spring;

import haiyan.orm.database.sql.TableDBContext;

public class TableDBContextUtils {

	private static ThreadLocal<TableDBContext> resource = new ThreadLocal<TableDBContext>();
	public static void set(TableDBContext context) {
		if (resource.get()!=null && resource.get()!=context) {
			remove();
			//throw new Warning("resource exists");
		}
		resource.set(context);
	}
	public static TableDBContext get() {
		return resource.get();
	}
	public static void remove() {
		resource.remove();
	}
}
