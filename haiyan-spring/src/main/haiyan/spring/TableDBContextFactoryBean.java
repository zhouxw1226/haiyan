//package haiyan.spring;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//
//import javax.sql.DataSource;
//
//import org.springframework.beans.factory.FactoryBean;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.context.ApplicationEvent;
//import org.springframework.context.ApplicationListener;
//import org.springframework.context.event.ContextClosedEvent;
//
//import haiyan.common.CloseUtil;
//import haiyan.common.exception.Warning;
//import haiyan.common.intf.database.sql.ISQLDatabase;
//import haiyan.common.intf.exp.IExpUtil;
//import haiyan.config.util.ConfigUtil;
//import haiyan.exp.ExpUtil;
//import haiyan.orm.database.sql.AbstractDBManagerFactory;
//import haiyan.orm.database.sql.MySqlDBManager;
//import haiyan.orm.database.sql.TableDBContext;
//import haiyan.orm.intf.session.ITableDBContext;
//
//public class TableDBContextFactoryBean extends AbstractDBManagerFactory
//		implements FactoryBean<ITableDBContext>, InitializingBean, ApplicationListener<ApplicationEvent> {
//	private DataSource dataSource;
//	public DataSource getDataSource() {
//		return dataSource;
//	}
//	public void setDataSource(DataSource dataSource) {
//		this.dataSource = dataSource;
//	}
//	static class MySqlDBManagerProxy extends MySqlDBManager {
//		protected MySqlDBManagerProxy(ISQLDatabase database) {
//			super(database);
//		}
//	}
//	private ITableDBContext context;
//	@Override
//	public ITableDBContext getObject() throws Exception {
//		final ITableDBContext context = new TableDBContext();
//		final IExpUtil exp = new ExpUtil(context);
//		context.setExpUtil(exp);
//		final ISQLDatabase database;
//		try {
//			database = (ISQLDatabase)createDatabase(ConfigUtil.getDefaultDSN());
//		} catch (Throwable e1) {
//			throw Warning.getWarning(e1);
//		}
//		context.setDBM(new MySqlDBManagerProxy(database){
//			@Override
//			public Connection getConnectionOnly() {
//				try {
//					return dataSource.getConnection();
//				} catch (SQLException e) {
//					throw Warning.getWarning(e);
//				}
//			}
//			@Override
//			public Connection getConnection() throws Throwable {
//				return getConnectionOnly();
//			}
//			/**
//			 * @return Connection
//			 * @throws Throwable
//			 */
//			@Override
//			public Connection getConnection(boolean openTrans) throws Throwable {
//				Connection conn = getConnectionOnly();
//				if (openTrans && this.autoCommit == false) // 指定要开启事务,例如执行insert update delete 或者 executeUpdate
//					if (this.isAlive() && conn.getAutoCommit()) { // 连接允许开启事务
//						conn.setAutoCommit(false);
//					}
//				return conn;
//			}
//			@Override
//			public void close() {
//				super.close();
//				CloseUtil.close(exp);
//				context.setExpUtil(null);
//			}
//		});
////		try {
////			((AbstractSQLDBManager)context.getDBM()).setConnection(dataSource.getConnection());
////		} catch (Throwable e) {
////			throw Warning.getWarning(e);
////		}
//		return context;
//	}
//	@Override
//	public Class<?> getObjectType() {
//		return ITableDBContext.class;
//	}
//	@Override
//	public boolean isSingleton() {
//		return false;
//	}
//	@Override
//	public void onApplicationEvent(ApplicationEvent event) {
//		System.out.println("onApplicationEvent:"+event);
//		if (event instanceof ContextClosedEvent) {
//			CloseUtil.close(context);
//		}
//	}
//	@Override
//	public void afterPropertiesSet() throws Exception {
//		System.out.println("afterPropertiesSet");
//	}
//}
