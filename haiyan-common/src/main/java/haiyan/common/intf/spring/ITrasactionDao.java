package haiyan.common.intf.spring;

import haiyan.orm.intf.session.ITableDBContext;

public interface ITrasactionDao {

	ITableDBContext getTableContext();
}
