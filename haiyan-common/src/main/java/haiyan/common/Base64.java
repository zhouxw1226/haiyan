package haiyan.common;

import java.io.IOException;

/**
 * @author zhouxw
 * 
 */
@SuppressWarnings("restriction")
public class Base64 {

	/**
	 * @param s
	 * @return String
	 */
	public static String getBASE64(byte[] bytes) {
		if (bytes == null)
			return null;
		return new sun.misc.BASE64Encoder().encode(bytes);
	}
	/**
	 * @param s
	 * @return bytes
	 * @throws IOException
	 */
	public static byte[] getFromBASE64(String s) throws IOException {
		if (s == null)
			return null;
		return new sun.misc.BASE64Decoder().decodeBuffer(s);
	}

}
