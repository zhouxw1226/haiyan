/*
 * Created on 2006-10-20
 *
 */
package haiyan.common;

import java.awt.Color;
import java.util.Random;

/**
 * @author zhouxw
 * 
 */
public class ColorUtil {

	/**
	 * 
	 */
	public ColorUtil() {
		super();
	}

	/**
	 * @param fc
	 * @param bc
	 * @return Color
	 */
	public static Color getRandColor(int fc, int bc) {
		// //
		Random random = new Random();
		if (fc > 255)
			fc = 255;
		if (bc > 255)
			bc = 255;
		int r = fc + random.nextInt(bc - fc);
		int g = fc + random.nextInt(bc - fc);
		int b = fc + random.nextInt(bc - fc);
		return new Color(r, g, b);
	}
}