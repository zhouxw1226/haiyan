/*
 * Created on 2007-2-8
 */
package haiyan.common;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.OutputStream;
import java.util.Random;

import javax.imageio.ImageIO;

import haiyan.common.intf.web.IWebContext;;

/**
 * @author zhouxw
 */
public class AffirmCode {

	/**
	 * Comment for <code>height width</code>
	 */
	private final static int width = 60, height = 20;
	/**
	 * Comment for <code>CODE_LENGTH</code>
	 */
	private final static int CODE_LENGTH = 4;
	/**
	 * Comment for <code>random</code>
	 */
	private Random random = null;
	/**
	 * Comment for <code>image</code>
	 */
	private BufferedImage image = null;
	/**
	 * Comment for <code>g</code>
	 */
	private Graphics g = null;
	/**
	 * Comment for <code>number</code>
	 */
	private int[] number = new int[CODE_LENGTH];
	/**
	 * 
	 */
	public AffirmCode() {
		random = new Random();
		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		g = image.getGraphics();
	}
	/**
	 * @param context
	 * @return String
	 */
	public String createCode(IWebContext context) {
		String sRand = "";
		for (int i = 0; i < CODE_LENGTH; i++) {
			number[i] = random.nextInt(10);
			sRand += number[i];
		}
		DebugUtil.debug(">put session_affirm_code:" + sRand);
		setAffirmCode(context, sRand);
		return sRand;
	}
	public static final String PARAM_AFFIRM_CODE = "AFFIRMCODE";
    public void setAffirmCode(IWebContext context,String code) {
//    	if (code==null) { // clear
//    		HttpSession session = context.getRequest().getSession();
//    		session.removeAttribute(PARAM_AFFIRM_CODE);
////    		this.delCookie(UserAuthenticatorFactory.PARAM_AFFIRM_CODE);
//    		return;
//    	}
//		HttpSession session = context.getRequest().getSession();
//		session.setAttribute(PARAM_AFFIRM_CODE, code);
    }
	/**
	 * @param context
	 */
	public String showCode(IWebContext context) {
		try {
			g.setFont(new Font("Times New Roman", Font.PLAIN, 18));
			g.setColor(ColorUtil.getRandColor(200, 250));
			g.fillRect(0, 0, width, height);
			g.setColor(Color.DARK_GRAY);
			g.drawRect(0, 0, width - 1, height - 1);
			for (int i = 0; i < 155; i++) {
				int x = random.nextInt(width);
				int y = random.nextInt(height);
				int xl = random.nextInt(12);
				int yl = random.nextInt(12);
				g.setColor(ColorUtil.getRandColor(160, 200));
				g.drawLine(x, y, x + xl, y + yl);
			}
			@SuppressWarnings("unused")
			String sRand = createCode(context);
			for (int i = 0; i < CODE_LENGTH; i++) {
				// g.setColor(new Color(20 + random.nextInt(110), 20 + random
				// .nextInt(110), 20 + random.nextInt(110)));
				g.setColor(ColorUtil.getRandColor(20, 110));
				g.drawString("" + number[i], 13 * i + 6, 16);
			}
			g.dispose();
			OutputStream output = null;
			try {
				output = context.getOutputStream();
//              context.setStatus(HttpServletResponse.SC_OK);
				ImageIO.write(image, "JPEG", output);
				output.flush();
			}
			finally {
				if (output != null)
					try {
						output.close();
					} catch (Throwable e) {
						e.printStackTrace();
					}
			}
			// context.write(sRand);
			// return sRand;
		} catch (Throwable ex) {
			ex.printStackTrace();
		}
		return "";
	}
	// OutputStream outStream = null;
	// FileInputStream fis = null;
	// try {
	// String fileName = DataConstant.APPLICATION_PATH
	// + "/comResource/images/affirmcode/" + sRand + ".jpg";
	// File file = new File(fileName);
	// if (!file.exists())
	// ImageIO.write(image, "JPEG", file);
	// //
	// fis = new FileInputStream(file);
	// // context.setHttpHeader(sRand + ".jpg", false);
	// // outStream = context.getOutputStream();
	// int bytesRead = -1;//
	// // byte[] buff = new byte[1024*8];
	// // while ((bytesRead = fis.read(buff)) != -1) {
	// // outStream.write(buff, 0, bytesRead);
	// // }
	// while ((bytesRead = fis.read()) != -1) {
	// outStream.write(bytesRead);
	// }
	// // ImageIO.write(image, "JPEG", outStream);
	// //
	// outStream.flush();
	// } catch (FileNotFoundException e) {
	// e.printStackTrace();
	// } catch (Exception e) {
	// e.printStackTrace();
	// } finally {
	// try {
	// if (fis != null)
	// fis.close();
	// if (outStream != null)
	// outStream.close();
	// //
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
}