package haiyan.common.security;

import java.lang.reflect.Constructor;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;

/**
 * 私钥加密数据
 * 
 * @author zhouxw
 * 
 */
public class EncryptData {

	private String keyfile = null;

	/**
	 * @param keyfile
	 */
	public EncryptData(String keyfile) {
		this.keyfile = keyfile;
	}

	/**
	 * @param keyfile
	 *            String
	 */
	public void setKeyFile(String keyfile) {
		this.keyfile = keyfile;
	}

//	/**
//	 * @param filename
//	 *            String
//	 * @param filenamekey
//	 *            String
//	 * @throws Throwable
//	 */
//	public void createEncryptData(String encryptfile, String filename)
//			throws Throwable {
//		if (keyfile == null || keyfile.equals("")) {
//			throw new NullPointerException("keyfile为空");
//		}
//		encryptData(encryptfile, filename);
//	}
//
//	/**
//	 * @param filename
//	 *            String
//	 * @param encryptfile
//	 *            String
//	 * @throws Throwable
//	 */
//	private void encryptData(String encryptfile, String filename)
//			throws Throwable {
//		byte data[] = DesUtil.readFile(filename);
//		byte encryptData[] = getEncryptData(data);
//		DesUtil.writeFile(encryptData, encryptfile);
//	}

	/**
	 * @param bytes
	 *            byte[]
	 * @return byte[]
	 * @throws Throwable
	 */
	public byte[] createEncryptData(byte[] bytes) throws Throwable {
		bytes = getEncryptData(bytes);
		return bytes;
	}

	/**
	 * @param bytes
	 * @return byte[]
	 * @throws Throwable
	 */
	private byte[] getEncryptData(byte[] bytes) throws Throwable {
		SecretKey key = null; // 密文
		// Cipher
		Cipher cipher = null;
		if (!DesUtil.getUseRandom()) {
			key = getKeyByRawKeyData(DesUtil.getAlgorithmKey().getBytes());
			IvParameterSpec spec = new IvParameterSpec(DesUtil.iv);
			// "Rijndael/CBC/PKCS5Padding" "DES/ECB/PKCS5Padding"
			cipher = Cipher.getInstance(DesUtil.getAlgorithm());
			cipher.init(Cipher.ENCRYPT_MODE, key, spec);
		} else {
			key = getKeyByRawKeyData(DesUtil.readFile(keyfile));
			SecureRandom sr = new SecureRandom();
			// Cipher
			cipher = Cipher.getInstance(DesUtil.getAlgorithm());
			cipher.init(Cipher.ENCRYPT_MODE, key, sr);
		}
		bytes = cipher.doFinal(bytes);
		return bytes;
	}

	/**
	 * @param rawKeyData
	 * @return SecretKey
	 * @throws Throwable
	 */
	private SecretKey getKeyByRawKeyData(byte[] rawKeyData) throws Throwable {
		Class<?> classkeyspec = Class.forName(DesUtil.getValue("keyspec"));
		Constructor<?> constructor = classkeyspec.getConstructor(new Class[] { byte[].class });
		KeySpec dks = (KeySpec) constructor.newInstance(new Object[] { rawKeyData });
		// new DESKeySpec(rawKeyData);
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DesUtil.getAlgorithmKeyType());
		return keyFactory.generateSecret(dks);
	}
	
}
