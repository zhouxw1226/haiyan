package haiyan.common.security;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import haiyan.common.DebugUtil;

/**
 * @author zhouxw
 * 
 */
public class DesUtil {

	//
	private static ResourceBundle resources = null;

	/**
	 * 暂时使用的一个固定密钥
	 */
	public static byte[] iv = new byte[] { 0x17, 0x2e, 0x66, 0x66, 0x14, 0x22, 0x41, 0x66 };

	/**
	 * 
	 */
	static {
		try {
			// System.out.println("#privatekey=" + iv);
			resources = ResourceBundle.getBundle("Algorithm", Locale.getDefault());
		} catch (Throwable ex) {
			// ex.printStackTrace();
			DebugUtil.error(ex);
		}
	}

	/**
	 * 
	 */
	private DesUtil() {
	}

	/**
	 * @param filename
	 * @throws IOException
	 * @return byte[]
	 */
	public static byte[] readFile(String filename) throws IOException {
		File file = new File(filename);
		if (filename == null || filename.equals("")) {
			throw new NullPointerException("...");
		}
		long len = file.length();
		byte[] bytes = new byte[(int) len];
		//
		BufferedInputStream bufferedInputStream = null;
		try {
			bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
			int r = bufferedInputStream.read(bytes);
			if (r != len)
				throw new IOException("...");
		} finally {
			if (bufferedInputStream != null)
				bufferedInputStream.close();
		}
		return bytes;

	}

	/**
	 * @param data
	 *            byte[]
	 * @throws IOException
	 */
	public static void writeFile(byte[] data, String filename)
			throws IOException {
		File file = new File(filename);
		if (!file.getParentFile().exists()) {
			if (!file.getParentFile().mkdirs()) {
				return;
			}
		}
		BufferedOutputStream bufferedOutputStream = null;
		try {
			bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
			bufferedOutputStream.write(data);
			bufferedOutputStream.flush();
		} finally {
			if (bufferedOutputStream != null)
				try {
					bufferedOutputStream.close();
				} catch (IOException e) {
					DebugUtil.error(e);
				}
		}
	}

	/**
	 * @param filename
	 *            String
	 * @throws IOException
	 * @return byte[]
	 */
	public byte[] readFileJar(String filename) throws IOException {
		BufferedInputStream bufferedInputStream = new BufferedInputStream(
				getClass().getResource(filename).openStream());
		int len = bufferedInputStream.available();
		byte[] bytes = new byte[len];
		int r = bufferedInputStream.read(bytes);
		if (len != r) {
			// bytes = null;
			throw new IOException("len error");
		}
		bufferedInputStream.close();
		return bytes;
	}

	/**
	 * @return String
	 */
	public static String getAlgorithm() {
		return resources.getString("algorithm");
	}

	/**
	 * @return String
	 */
	public static String getAlgorithmKeyType() {
		return resources.getString("algorithm-keytype");
	}

	/**
	 * @return String
	 */
	public static String getAlgorithmKey() {
		return resources.getString("algorithm-key");
	}

	/**
	 * @param skey
	 * @return String
	 */
	public static String getValue(String skey) {
		return resources.getString(skey);
	}

	/**
	 * @return boolean
	 */
	public static boolean getUseRandom() {
		return Boolean.valueOf(resources.getString("userandom"));
	}

	/**
	 * @deprecated 仅作显示用
	 * @param b
	 * @return String
	 */
	public static String byte2hex(byte[] b) { 
		String hs = "";
		String stmp = "";
		for (int n = 0; n < b.length; n++) {
			stmp = (java.lang.Integer.toHexString(b[n] & 0XFF));
			if (stmp.length() == 1)
				hs = hs + "0" + stmp;
			else
				hs = hs + stmp;
			if (n < b.length - 1)
				hs = hs + ":";
		}
		return hs.toUpperCase();
	}

}
