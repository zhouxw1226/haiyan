package haiyan.orm.database;

import haiyan.common.DebugUtil;
import haiyan.orm.intf.database.ICacheDBManager;

public abstract class AbstractCacheDBManager implements ICacheDBManager {
//	public boolean isDebug() {
//		return false;
//	}
	protected void print(String s) {
//		if (this.isDebug())
//			DebugUtil.debug(s);
//		else
			DebugUtil.info(s);
	}
}