package haiyan.database;

import java.sql.Connection;
import java.util.concurrent.atomic.AtomicInteger;

import haiyan.common.DebugUtil;
import haiyan.common.exception.Warning;
import haiyan.common.intf.database.sql.ISQLDatabase;

/**
 * SQLDatabase abstraction
 * 
 * @author zhouxw
 * 
 * @created 22. mars 2002
 * @todo Make use of registry prefs optional
 */
public abstract class SQLDatabase implements ISQLDatabase {
	public volatile static AtomicInteger connCount = new AtomicInteger(0); // 4 debug
	@Override
	public final Connection getConnection(String user,String pass) {
		try {
			Connection conn = getDBConnection(user,pass);
			connCount.addAndGet(1);
			DebugUtil.debug(">----< db.open.connHash:"+conn.hashCode()+"\tdb.connCount:"+connCount);
			return conn;
		} catch (Throwable e) {
			throw Warning.getWarning(e);
		}
	}
	@Override
	public final Connection getConnection() {
		try {
			Connection conn = getDBConnection();
			connCount.addAndGet(1);
			DebugUtil.debug(">----< db.open.connHash:"+conn.hashCode()+"\tdb.connCount:"+connCount);
			return conn;
		} catch (Throwable e) {
			throw Warning.getWarning(e);
		}
	}
	protected abstract Connection getDBConnection(String user,String pass) throws Throwable;
	protected abstract Connection getDBConnection() throws Throwable;
	@Override
	public abstract String getLabel();
	@Override
	public abstract String getURL() throws Throwable;
	private String dbType;
	@Override
	public String getDBType() {
		return dbType;
	}
	@Override
	public void setDBType(String dbType) {
		this.dbType = dbType;
	}
	private String DSN;
	@Override
	public String getDSN() {
		return DSN;
	}
	@Override
	public void setDSN(String dsn) {
		DSN = dsn;
	}
}